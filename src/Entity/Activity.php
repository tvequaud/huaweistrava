<?php


namespace App;


class Activity
{
    const TYPE_WALK = 'Walk';
    const TYPE_RUN = 'Run';
    const TYPE_CYCLE = 'Cycle';
    const TYPE_POOL_SWIM = 'Swim_Pool';
    const TYPE_OPEN_WATER_SWIM = 'Swim_Open_Water';
    const TYPE_HIKE = 'Hike';
    const TYPE_INDOOR_RUN = 'Indoor_Run';
    const TYPE_INDOOR_CYCLE = 'Indoor_Cycle';
    const TYPE_CROSS_TRAINER = 'Cross_Trainer';
    const TYPE_OTHER = 'Other';
    const TYPE_CROSSFIT = 'CrossFit';
}
