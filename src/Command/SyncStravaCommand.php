<?php

namespace App\Command;

use PhpParser\JsonDecoder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class SyncStravaCommand extends Command
{
    protected static $defaultName = 'app:sync-strava';

    /** @var string */
    private $zipFilePath;

    protected function configure()
    {
        $this
            // Help
            ->setDescription('Export data from HuaweiHealth into TCX files for Strava')
            ->setHelp('1. Export your personal data from HuaweiHealth ; then 2. Convert those data into TCX files ; finally 3. Import TCX files in your Strava account')
            // Args
            ->addArgument('zip', InputArgument::REQUIRED, 'ZIP data Huawei Health')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Init
        $zipFile = $input->getArgument('zip');

        $filesystem = new Filesystem();
        if (!$filesystem->exists($zipFile)) {
            $output->writeln("File $zipFile does not exist");
            return Command::FAILURE;
        }

        $output->writeln("Extract $zipFile");
        $zip = new \ZipArchive();
        if ($zip->open($zipFile) != true) {
            $output->writeln("Cannot open Zip file $zipFile");
            return Command::FAILURE;
        }

        // Extract
        $tmpDir = sys_get_temp_dir() . '/' . uniqid('HuaweiStrava_');
        if ($zip->extractTo($tmpDir)) {
            $output->writeln("ZIP extracted to $tmpDir");
        }
        $zip->close();

        // Deserializer from JSON to object
        $jsonFile = "$tmpDir/Motion path detail data & description/motion path detail data.json";
        $activities = json_decode(file_get_contents($jsonFile));

//102 = natation_open_water
//4 = course exterieure
        foreach ($activities as $activity) {
            $data = [
                'recordDay' => $activity->recordDay,
            ];
            foreach ($activity->motionPathData as $motionPathData) {
                $data = array_merge($data, [
                    'sportType' => $motionPathData->sportType,
                    'totalTime' => $motionPathData->totalTime,
                    'timeZone' => $motionPathData->timeZone,
                    'totalCalories' => $motionPathData->totalCalories,
                    'startTime' => $motionPathData->startTime,
                    'endTime' => $motionPathData->endTime,
                    'gps' => null,//$motionPathData['attribute']['HW_EXT_TRACK_DETAIL'],
                    'stats' => null,//$motionPathData['attribute']['HW_EXT_TRACK_SIMPLIFY'],
                ]);
//'totalSteps' => $motionPathData->totalSteps, // depends on sportType
//'totalDistance' => $motionPathData->totalDistance,
//'paceMap' => $motionPathData->paceMap, // useless ? depends on sportType
//'partTimeMap' => $motionPathData->partTimeMap, // useless ? depends on sportType
            }
            dump($data);
        }

        // Serializer from object to TCX (XML)

        // Done
        $filesystem->remove($tmpDir);
        $output->writeln('TCX files exported');
        return Command::SUCCESS;
    }
}
